# Programación en python para biología 2019
**Universidad Favaloro**
* Sede: Sarmiento 1853
* Cursado: 8 clases los viernes de 13 a 17hs.
* Período: desde el 6 de septiembre hasta el 25 de octubre.
* Aula: a definir
###   Links Útiles:
* :computer: [Cómo instalar la Máquina Virtual](COMO_INSTALAR_LA_VM.md)
* :cd:[Instalar Programas Necesarios](https://gitlab.com/nmoreyra/python-in-biology/blob/master/INSTALAR_PROGRAMAS_NECESARIOS.md)
___
## Contenidos del curso
* #### Clase I. Configuración inicial
	* [Introducción a la Bioinformática. Desarrollo de software para ciencias biológicas.](https://docs.google.com/presentation/d/1sg0wpm9P-53uyDzV0UZRHZUmPI7DOX6ZLlJyViEV5-k/edit?usp=sharing)
	* [Unix/Linux: organización, arquitectura, sistema de archivos.](Clase_I/IntroduccionLinux.pdf) :penguin:
	* La terminal de Unix. ![](Clase_I/imgs/terminal2.png)
	* [Introducción a la terminal.](https://swcarpentry.github.io/shell-novice-es/01-intro/index.html)
		*  [Planilla de resumen de comandos básicos](Clase_I/Linux-comandos-basicos.pdf)
		* [Navegación de archivos y directorios.](https://swcarpentry.github.io/shell-novice-es/02-filedir/index.html)
		* [Trabajando con archivos y directorios.](https://swcarpentry.github.io/shell-novice-es/03-create/index.html)
		* [*Pipes* y filtros](https://swcarpentry.github.io/shell-novice-es/04-pipefilter/index.html)
		* [Bucles](https://swcarpentry.github.io/shell-novice-es/05-loop/index.html)
		* [Scripts de la terminal](https://swcarpentry.github.io/shell-novice-es/06-script/index.html)
	* Python: fundamentos, versiones y distribuciones, instalación y ejecución, instalación de librerías, ayuda disponible.
		* [Diapositivas "Introducción a Python"](Clase_I/Introducción_a_Python.pdf)
		* [Invocando al intérprete](http://docs.python.org.ar/tutorial/3/interpreter.html#invocando-al-interprete)
		* [Breve introducción a la Programación en Python](https://datacarpentry.org/python-ecology-lesson-es/01-short-introduction-to-Python/index.html)
	* Primer programa.
	* Guías, tutoriales y cursos de la Clase I.  
                *  [Las mejores guias de python.](http://www.python.org.ar/wiki/AprendiendoPython)
		*  [Codecademy.](https://www.codecademy.com/catalog/subject/all)
		*  [Lista de libros de programación I](https://github.com/EbookFoundation/free-programming-books/blob/master/free-programming-books.md)
		*  [Lista de libros de programación II](https://github.com/EbookFoundation/free-programming-books/blob/master/free-programming-books-es.md)
		*  [Curso de python online.](https://codigofacilito.com/courses/Python)
		*  [Otro curso de python.](https://teachlr.com/cursos-online/curso-basico-de-python/)
		* [Instalación de software y configuración de entorno de trabajo local y online.](https://www.digitalocean.com/community/tutorials/how-to-install-python-3-and-set-up-a-local-programming-environment-on-ubuntu-16-04)
		*  [Página de cursos de todo un poco.](https://www.tareasplus.com/)



----
* #### Clase II. Scripting y control de versiones
	 * [Scripting en Linux (Shell/Bash)](https://www.profesionalreview.com/2017/03/12/shell-script-linux/)
		 * [Blast_local](Clase_II/Blast_local.md)
		 * [Blast_script](Clase_II/Blast_script.md)
	 * [Grep, sed, awk](https://gitlab.com/nmoreyra/python-in-biology/blob/master/Clase_II/grep-sed-awk.md)
	 * [Control de versiones con Git:](https://swcarpentry.github.io/git-novice-es/)
		 * Control Automatizado de Versiones
		 * Configurando Git
		 * Creando un repositorio
		 * Rastreando Cambios
		 * Explorando el "History"
		 * Ignorando cosas
		 * Repositorios remotos en GitHub
		 * Trabajos en colaboración
		 * Conflictos
	 * [Git local y remoto.](https://swcarpentry.github.io/git-novice-es/07-github/index.html)
	 * [Trabajo colaborativo](https://swcarpentry.github.io/git-novice-es/08-collab/index.html)
	 * Guías, tutoriales y cursos de la Clase II
	         * [Programación de Shell Scripts (pdf)](https://www.fceia.unr.edu.ar/~diegob/so/presenta/02-ShellScripts.pdf)
		 * [Linux Shell Scripting Cookbook (pdf)](https://gutl.jovenclub.cu/wp-content/uploads/2013/10/Linux.Shell_.Scripting.Cookbook.pdf)
		 * [Linux Shell Scripting Tutorial Ver. 1.0 (pdf)](https://www.kau.edu.sa/files/830/files/60761_linux.pdf)
		 * [Linux Shell Scripting Tutorial v2.0 (pdf)](http://docs.linuxtone.org/ebooks/Shell/Linux%20Shell%20Scripting%20Tutorial%20v2.0.pdf)
		 * [Shell Scripting Tutorial](https://www.shellscript.sh/)
		 * [Bash Reference Manual (pdf)](https://www.gnu.org/software/bash/manual/bash.pdf)
		 * [Unix Shell Scripting Tutorial (pdf)](https://supportweb.cs.bham.ac.uk/docs/tutorials/docsystem/build/tutorials/unixscripting/unixscripting.pdf)
		 * [Bash Guide for Beginners (pdf)](https://www.tldp.org/LDP/Bash-Beginners-Guide/Bash-Beginners-Guide.pdf)

----
 * #### Clase III. Python: Variables y control de flujo
	 * Sintaxis general
	 * [Guías de estilo: PEP8](https://www.python.org/dev/peps/pep-0008/#maximum-line-length) 
	 * Definición y uso de variables
	 * Operadores: matemáticos y relacionales
	 * Tipos de objetos básicos: int, float, string, lista, diccionario, set, tupla

		 * *Guías, tutoriales y cursos de la Clase III*
		    * [Guia completa sintaxis Python (pdf)](https://www.iaa.csic.es/python/curso-python-para-principiantes.pdf)
		    * [Manual Python (pdf)](http://docs.python.org.ar/tutorial/pdfs/TutorialPython2.pdf)

Estructuras de control: if, elif, for, while.
----
* #### Clase IV. Python: Funciones, manejo de archivos e interpretación de errores
	Definición y uso de funciones.
	Manipulación de texto:
	Operaciones y métodos útiles sobre cadenas de texto
	Conversión entre tipos de datos
	Expresiones regulares
	Lectura y escritura de archivos.
	Pase de argumentos a programas.
	Ejecución de programas externos.
	Interpretación de errores.
	Documentación de programas.
	Entornos de trabajo.
----
* #### Clase V. Python: Manejo de datos
	Tablas de datos con Pandas:
Lectura y escritura de datos estructurados.
Datos en grupos.
Datos faltantes.
Estadística descriptiva.
----
* #### Clase VI. Python: Visualización de datos
	Generación de gráficos con Matplotlib, Seaborn, ggplot:
Gráficos de dispersión
Gráficos de barra
Gráficos de líneas
Gráficos de caja y bigote
Histogramas
Mapas de calor
Personalización de gráficos.
Gráficos para publicación.
----
* #### Clase VII. Python para Biología
	Análisis de datos biológicos: Biopython. Manejo de datos genómicos (genomas, anotaciones, lecturas de secuenciación en formato fastq, etc) para el cálculo de estadísticas básicas (estándares).
Scipy: métodos estadísticos.
Filogenia: Bio.Phylo; ETE Toolkit.
----
* #### Proyecto supervisado: Análisis e Interpretación de datos
	Planteo de proyectos individuales.
Formulación de hipótesis y esquema de trabajo.
Obtención de datos crudos.
Limpieza de datos.
Conversión a formatos procesables.
Estadística descriptiva y avanzada.
Generación e interpretación de gráficos.
Interpretación de resultados.
Puesta en común de proyectos: factibilidad de análisis, sugerencias.
Conclusiones generales.
Directivas para informe final.
----
* #### Bibliografía optativa
 * [VV. AA. Software Carpentry: La Terminal de Unix.](https://swcarpentry.github.io/shell-novice-es/)
 * [VV. AA. Software Carpentry: El Control de Versiones con Git.](https://swcarpentry.github.io/git-novice-es/)
 * [Ekmekci B, McAnany CE, Mura C. An Introduction to Programming for Bioscientists: A Python-Based Primer. PLoS Comput Biol. 2016 Jun 7;12(6):e1004867.](http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1004867)
 * [Bahit E. Curso: Python para Principiantes.](https://www.iaa.csic.es/python/curso-python-para-principiantes.pdf)
 * [Jones M. Python for Biologists.](http://pythonforbiologists.com/)
 * [White E. Programming for Biologists.](http://www.programmingforbiologists.org/material/)
 * [Pratusevich M. Practice Python.](http://www.practicepython.org/)
 * [Kirienko D. et al. Snakify.](http://snakify.org/)
 * [Müller M. Scientific Plotting with Matplotlib.](http://www.python-academy.com/download/pycon2012/matplotlib_handout.pdf)
 * [Varios autores. Matplotlib Documentation.](http://matplotlib.org)
 * [Varios autores. Seaborn Documentation.](http://seaborn.pydata.org)
 * [McKinney W, PyData Development Team. Pandas: powerful Python data analysis toolkit.](https://pandas.pydata.org/pandas-docs/stable/pandas.pdf)
 * [Varios autores. Pandas Documentation.](https://pandas.pydata.org/pandas-docs/stable/)
 * [Varios autores. Biopython Documentation.](http://biopython.org/wiki/Documentation)
 * [Varios autores. Biopython Tutorial and cookbook.](http://biopython.org/DIST/docs/tutorial/Tutorial.pdf)
 * [Vos R. Bio::Phylo Documentation.](https://informatics.nescent.org/w/images/5/57/BioPhylo.pdf)
 * [Huerta-Cepas J, Serra F, Bork P. ETE Toollkit.](http://etetoolkit.org/)
 * [Varios Autores. Lasagne documentation.](https://media.readthedocs.org/pdf/lasagne/latest/lasagne.pdf)
 * [Varios autores. Python Enhancement Proposals (PEPs).](https://www.python.org/dev/peps/)
