# Filtros grep, sed y awk

## grep

Realizar búsquedas dentro de un archivo es posible utilizando el comando `grep`. Este comando toma como argumento el patrón de tú interés y el nombre (ruta) del archivo en el que deseas realizar la búsqueda.
Para demostrar algunas de sus funcionalidades, vamos a utilizar nuevamente el archivo `/home/alumno/Clase_II/secuencias_blast/uniprot_human_proteins_reviewed.fasta`. Éste contiene múltiples secuencias, cada una de los cuales está precedida por una línea que contiene información relacionada (IDs).
Chekee que la existencia del contenido y que no se encuentre vacío:

`ls -l secuencias_blast/uniprot_human_proteins_reviewed.fasta` (no utilizamos una ruta absoluta, sino una relativa porque ya nos encontramos en /home/alumno/Clase_II ).
`head Sequences.fasta`

Ahora suponga que queremos saber si los códigos de identificación de las secuencias corresponden a "Vaccinia virus". Para esto, podemos buscar ese patrón con el comando `grep`.

`grep 'Vaccinia virus' secuencias_blast/uniprot_human_proteins_reviewed.fasta`

Como verá, este comando dió como salida mucha información en líneas, las cuales son difíciles de leer de esa manera. Además, surgen dos interrogantes:
1) ¿cómo puedo almacenar esa salida de la búsqueda con grep?
Si quisieramos que todas aquellas líneas que coincidan con el patrón, en este caso fragmentos de identificadores de secuencias, sean guardadas en un archivo para enviarselo a alguien más, podemos hacer lo siguiente:

`grep 'Vaccinia virus' secuencias_blast/uniprot_human_proteins_reviewed.fasta > IDs-proteins_to_Vaccinia-virus.txt`


2) ¿Cómo puedo, antes de almacenar la salida, saber sólo cuantas líneas cumplen con el patrón de búsqueda?
grep tiene otra gran funcionalidad, puede contar la cantidad de líneas que coinciden con el patrón deseado.
En este caso, le estamos diciendo a grep con el argumento `-c` que cuente el número de líneas donde aparece el patrón y sólo nos retorne ese valor, que en este ejemplo será 367.

`grep -c 'Vaccinia virus' secuencias_blast/uniprot_human_proteins_reviewed.fasta`


Por último, revise rápidamente el manual de este comando y vea qué otras tareas pueden realizarse.

`grep --help`


### Información adicional:
El nombre **grep** viene de “global regular expression print” ó " g/re/p "
Es un comando muy útil y potente que tiene, además, dos variantes que componen la familia: `egrep` y `fgrep`. En este curso sólo trabajaremos con `grep` pero los alentamos a inspeccionar sus variantes.
Este comando es la respuesta a esos momentos donde sabes que quieres una frase específica de un archivo pero no puedes recondar su nombre (parcial o totalmente). grep usa expresiones regulares (que veremos más adelante) para buscar coincidencias de patrones.
En linux se utilizan muchos caracteres especiales por medio del shell, denominados "metacaracteres", que al ser utilizados en expresiones regulares tienen un significado diferente que como patrón de texto. Por este motivo, es un buen hábito utilizar siempre comillas cuando se detalla el patrón (texto que queremos buscar) como argumento.

Escapado de Caracteres especiales.
Incluso cuando utilizamos comillas en la expresión regular, la shell no interpretará correctamente algunos metacaracteres (por ejemplo, * y .). En este caso, la alternativa para utilizarlos textualmente es escaparlos con la barra invertida `\`, denominada backslash en inglés.
Un ejemplo para que quede más claro. Supongamos que desea buscar el patrón "a*b*", salvo que hagamos un escape del asterisco (*), nuestra búsqueda no servirá.
Si utilizamos \ para escapar este caracter, podremos buscar correctamente:

`grep "a\*b\*" file_to_search.txt`

Existe muchísima más información sobre el uso de grep y, en particular, sobre expresiones regulares y su pontencialidad. Sin embargo, hasta acá llegaremos por el momento e iremos agregando conceptos y ejemplos durante el resto del curso.

## 
## sed (Stream-oriented, Non-Interactive, Text Editor)
Sed es considerado un editor de texto orientado a "flujo" (en contraposición a los clásicos editores clásicos). sed acepta como entrada un archivo o la salida de otro comando y procesa de a una línea a la vez, y el resultado es enviado a la salida estándar (por la terminal de comandos).
Busca patrones en las líneas del _input_, al igual que grep, pero puede cambiar el contenido de las líneas que cumplan con el patrón de búsqueda.

### Formato de uso
El formato básico de uso de sed es:
`sed [-ns] '[direccion] instruccion argumentos' `

Donde:

* **[direccion]** es opcional, siendo un número de línea (N), rango de números de línea (N,M) o búsqueda de expresion regular (/cadena/) indicando el ámbito de actuación de las instrucciones. Si no se especifica **[direccion]**, se actúa sobre todas las líneas del flujo.
* Instruccion puede ser:
	* i = Insertar línea antes de la línea actual.
	* a = Insertar línea después de la línea actual.
	* c = Cambiar línea actual.
	* d = Borrar línea actual.
	* p = Imprimir línea actual en stdout.
	* s = Sustituir cadena en línea actual.
	* r fichero = Añadir contenido de "fichero" a la línea actual.
	* w fichero = Escribir salida a un fichero.
	* ! = Aplicar instrucción a las líneas no seleccionadas por la condición.
	* q = Finalizar procesamiento del fichero.
	* -n: No mostrar por stdout las líneas que están siendo procesadas.
	* -s: Tratar todos los ficheros entrantes como flujos separados.

### Ejemplos de su uso

Sustituir apariciones de cadena1 por cadena2 en todo el fichero:
`sed 's/cadena1/cadena2/g' fichero > fichero2`

Sustituir apariciones de cadena1 por cadena2 en las líneas 1 a 10:
`comando | sed '1,10 s/cadena1/cadena2/g'`
  
Eliminar las líneas 2 a 7 del fichero
`sed '2,7 d' fichero > fichero2`
  
Buscar un determinado patrón en un fichero:
`sed -e '/cadena/ !d' fichero`
  
Buscar AAA o BBB o CCC en la misma línea:
`sed '/AAA/!d; /BBB/!d; /CCC/!d' fichero`

Buscar AAA y BBB y CCC:
`sed '/AAA.*BBB.*CCC/!d' fichero`


### Información adicional:


##  
## awk
El comando awk permite imprimir una columna específica de un archivo. Por ejemplo, para imprimir la segunda y tercera columna del archivo "dataset.txt":

awk '{print $2" "$3}' dataset.txt


Tenga en cuenta que necesita prestar atención al caracter de delimitación utilizado en el archivo. En este caso, el archivo es tabulado, lo cual es detectado automáticamente por awk.$
si el archivo estuviera delimitado por comas, awk no sería capaz de diferenciar entre columnas.

El archivo ‘dataset.csv’ contiene la misma información que dataset.txt pero está delimitado por comas.

head dataset.csv

Por lo tanto, para utilizar awk con este archivo, se debe indicar cuál es el caracter de delimitación. Esto se hace utilizando el argumento ‘-F’:

awk -F "," '{print $2" "$3}' dataset.csv
