## Probando blast de forma local.

Si googleamos `blast linux download` problablemente el primer resultado sea la dirección desde donde podemos descargar los archivos ejecutables para correr blast en nuestras computadoras ([Descargar Blast+](https://blast.ncbi.nlm.nih.gov/Blast.cgi?PAGE_TYPE=BlastDocs&DOC_TYPE=Download)). Sin embargo, Uds ya cuentan con el software instalado en sus ordenadores (VM), donde tienen disponibles todas las aplicaciones, incluso aquellas necesarias para formatear las bases de datos a utilizar antes de realizar el **blast**. Cualquier programa puede ser ejecutado simplemente llamando su nombre desde la terminal, como se verá más adelante.

La carpeta `secuencias_blast`, ubicada en la carpeta `Clase_II`, posee el archivo `uniprot_human_prot_rev.fasta`, que contiene alrededor de 39.000 secuencias proteicas humanas obtenidas de Uniprot (http://www.uniprot.org/). El archivo `query.fas`, posee una secuencia de nucleótidos de mRNA expresada en un tejido de cáncer de pulmón humano. Antes de "blastear", es necesario crear una base de datos a partir de las secuencias contra las cuales se quiere buscar nuestra secuencia de consulta.

Estando ubicado sobre la carpeta `~/Clase_II/`, ver el contenido de la carpeta `secuencias_blast`. Luego, crear la base de datos dentro de la carpeta `trabajo` mediante el comando `makeblastdb`.

```bash
ls secuencias_blast
mkdir trabajo #creamos una carpeta donde trabajar y colocar las salidas de las corridas de `blast`
makeblastdb -in secuencias_blast/uniprot_human_proteins_reviewed.fasta -dbtype prot -out trabajo/db_prot_human
```

`blastx` es un programa que traduce la secuencia query (que es nucleotídica) en sus seis posibles marcos de lectura (tres marcos de lecturas por hebra) y compara estas secuencias traducidas contra una base de datos de proteínas.
Utilizar `blastx` y explorar los resultados, ¿existe algún hit a una secuencia en particular? ¿Esa secuencia tiene semejanza a una proteína humana? ¿Es posible presentar los resultados en formato de tabla?

```bash
blastx -db trabajo/db_prot_human -query secuencias_blast/query.fas -out trabajo/salida.blastx
```

Investigar el archivo de salida. ¿Qué información se obtiene?

```bash
less -S trabajo/salida.blastx
```

*Hint: investigar los diferentes formatos de salidas de blast (blastx -help)*